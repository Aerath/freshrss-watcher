const DEFAULT_URL = "https://freshrss.example.com/";
const DEFAULT_USER = "user";
const DEFAULT_PASSWORD = "password";
const DEFAULT_INTERVAL = 10;
const DEFAULT_BGCOLOR = "#690606";

// Handle generic error
function handleError(error) {
    browser.browserAction.setTitle({title: browser.i18n.getMessage("errorId", error.message)});
    browser.browserAction.setIcon({path: "icons/error.svg"});
    browser.browserAction.setBadgeText({text: ""});
};

// Check HTTP response code
function checkHttpResponseCode(code) {
    if (code == "200") {
        return true;
    }

    if (code == "401") {
        browser.browserAction.setTitle({title: browser.i18n.getMessage("authErrorId")});
        browser.browserAction.setIcon({path: "icons/error.svg"});
        browser.browserAction.setBadgeText({text: ""});
        return false;
    }

    if (code == "404") {
        browser.browserAction.setTitle({title: browser.i18n.getMessage("urlErrorId")});
        browser.browserAction.setIcon({path: "icons/error.svg"});
        browser.browserAction.setBadgeText({text: ""});
        return false;
    }

    browser.browserAction.setTitle({title: browser.i18n.getMessage("updateErrorId", response.statusText)});
    browser.browserAction.setIcon({path: "icons/error.svg"});
    browser.browserAction.setBadgeText({text: ""});
    return false;
}

// Get authentication token
function getToken() {
    let getSettings = browser.storage.local.get("settings");
    getSettings.then((result) => {
        if (result.settings.url !== DEFAULT_URL) {
            var apiUrl = result.settings.url + "api/greader.php/accounts/ClientLogin?"
            var authUrl = apiUrl + "Email=" + result.settings.login + "&Passwd=" + result.settings.password;
            var request = new Request(authUrl);
            fetch(request).then(analyzeTokenResponse).catch(handleError);
        }
    })
};

// Analyze authentication response
function analyzeTokenResponse(response) {
    // Check for successful response
    if (checkHttpResponseCode(response.status)) {
        response.text().then((body) => {
            let authentication = {
                token: 0
            }

            var auth = /Auth=.+\/([a-z0-9]+)/.exec(body);
            if (!auth) {
                browser.browserAction.setTitle({title: browser.i18n.getMessage("authErrorId")});
                browser.browserAction.setIcon({path: "icons/error.svg"});
                browser.browserAction.setBadgeText({text: ""});
            }
            else {
                authentication.token = auth[1];
            }
            let setAuthentication = browser.storage.local.set({authentication});
        }).then(getUnreadCount);
    }
};

// Get number of unread articles
function getUnreadCount() {
    let getSettings = browser.storage.local.get(["settings", "authentication"]);
    getSettings.then((result) => {
        if ((result.settings.url !== DEFAULT_URL) && (result.authentication.token != 0)) {
            var authLogin = result.settings.login + "/" + result.authentication.token;
            var authHeaders = new Headers();
            authHeaders.append("Authorization", "GoogleLogin auth=" + authLogin);

            var unreadCountUrl = result.settings.url + "api/greader.php/reader/api/0/unread-count?output=json";

            var request = new Request(unreadCountUrl, {headers: authHeaders});
            fetch(request).then(analyzeUnreadResponse).catch(handleError);
        }
    }).catch(handleError);
};

// Analyze response to unread count request
function analyzeUnreadResponse(response) {
    // Check for successful response
    if (checkHttpResponseCode(response.status)) {
        response.text().then((body) => {
            var obj = JSON.parse(body);
            var unread = obj.max;
            if (isNaN(unread)) unread = 0;

            if (unread == 0) {
                var zero = browser.i18n.getMessage("zeroCountId")
                browser.browserAction.setTitle({title: browser.i18n.getMessage("unreadCountId", zero)});
                browser.browserAction.setIcon({path: "icons/normal.svg"});
                browser.browserAction.setBadgeText({text: ""});
            } else if (unread > 0) {
                browser.browserAction.setTitle({title: browser.i18n.getMessage("unreadCountId", unread.toString())});
                browser.browserAction.setIcon({path: "icons/alert.svg"});
                browser.browserAction.setBadgeText({text: unread.toString()});
            } else {
                browser.browserAction.setTitle({title: browser.i18n.getMessage("errorId", body)});
                browser.browserAction.setIcon({path: "icons/error.svg"});
                browser.browserAction.setBadgeText({text: ""});
            };
        });
    }
};

// Callback function called after installation/update
function handleInstall(details) {
    if (details.reason == "install") {
        //  The extension was installed, initialize the settings and token
        let settings = {
            url: DEFAULT_URL,
            login: DEFAULT_USER,
            password: DEFAULT_PASSWORD,
            updateInterval: DEFAULT_INTERVAL,
            bgColor: DEFAULT_BGCOLOR
        }
        let authentication = {
            token: 0
        }
        let setSettings = browser.storage.local.set({settings, authentication});

        browser.notifications.create("install-notification", {
            type: "basic",
            message: browser.i18n.getMessage("InstallNotificationMessage"),
            iconUrl: "icons/alert.svg",
            title: "FreshRSS Watcher"
        });
        browser.notifications.onClicked.addListener(function(notificationId) {
            browser.runtime.openOptionsPage();
        });
    } else if (details.reason == "update") {
        // The extension was updated, notify it
        var manifest = browser.runtime.getManifest();
        browser.notifications.create("update-notification", {
            type: "basic",
            message: browser.i18n.getMessage("UpdateNotificationMessage", manifest.version),
            iconUrl: "icons/alert.svg",
            title: "FreshRSS Watcher"
        });
        browser.notifications.onClicked.addListener(function(notificationId) {
            browser.tabs.create({url:"https://framagit.org/dohseven/freshrss-watcher", active: true});
        });
    }
}

// Callback function called on click on extension
function handleClick() {
    getToken();

    let getSettings =  browser.storage.local.get("settings");
    getSettings.then(result => {
        if (result.settings.url !== DEFAULT_URL) {
            var querying = browser.tabs.query({url: result.settings.url + "*"});
            querying.then((tabs) => {
                if (tabs.length > 0) {
                    // Tab is already opened, give it focus
                    browser.tabs.update(tabs[0].id, {active: true})
                } else {
                    // Open a new tab
                    browser.tabs.create({url:result.settings.url, active:true});
                }});
        } else {
            // The FreshRSS URL has not been configured yet, go to options page
            browser.runtime.openOptionsPage();
        }
    }).catch(handleError);
};

// Function used to reset the periodic watcher and chech unread articles count
function resetPeriodicWatcher() {
    let getSettings =  browser.storage.local.get("settings");
    getSettings.then(result => {
        if (result.settings) {
            browser.alarms.clear("checkUnreadCount");
            var interval = result.settings.updateInterval;
            browser.alarms.create("checkUnreadCount", {delayInMinutes:interval, periodInMinutes:interval});
            browser.browserAction.setBadgeBackgroundColor({color:result.settings.bgColor});
            browser.alarms.onAlarm.addListener(getToken);
            getToken();
        }
    }).catch(handleError);
}

// Callback function called when a message is received
function handleMessage(message) {
    if (message.action == "settingsUpdated") {
        resetPeriodicWatcher();
    }
}

// Callback function called when a tab is closed
function handleSessionChange(tabId) {
    // Get last closed tab
    var gettingSessions = browser.sessions.getRecentlyClosed({
        maxResults: 1
    });
    gettingSessions.then(sessionInfos => {
        if (!sessionInfos.length) {
            return;
        }
        let sessionInfo = sessionInfos[0];
        if (sessionInfo.tab) {
            let getSettings =  browser.storage.local.get("settings");
            getSettings.then(result => {
                // Check if closed tab was the FreshRSS one
                if (sessionInfo.tab.url.indexOf(result.settings.url) !== -1) {
                    resetPeriodicWatcher();
                }
            }).catch(handleError);
        }
    });
}

// Add the listeners
browser.runtime.onInstalled.addListener(handleInstall);
browser.browserAction.onClicked.addListener(handleClick);
browser.runtime.onMessage.addListener(handleMessage);
browser.sessions.onChanged.addListener(handleSessionChange)

// Launch periodic watcher
resetPeriodicWatcher();
