# FreshRSS Watcher

> An open source Firefox extension to be notified of unread articles in your FreshRSS instance.

## About

FreshRSS Watcher allows you to monitor the number of unread articles in your FreshRSS instance.
A click on the extension icon opens your FreshRSS page so you can read the articles.

## Acknowledgments

This extension is based on [ttrss-watcher](https://bitbucket.org/meinfuchs/ttrss-watcher/).
